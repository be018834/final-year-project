# -*- coding: utf-8 -*-
"""Clustering_FYP(Working).ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1qYMGWtELk9UChIdoJiUUq8Fst2YctwGI
"""

!pip install stop-words
!python -m nltk.downloader punkt
!python -m nltk.downloader averaged_perceptron_tagger

pip install -U sentence-transformers

import pandas as pd
import csv
import numpy as np
import pickle
import datetime
import matplotlib.pyplot as plt
import nltk
from nltk.tokenize import word_tokenize
nltk.download('wordnet')
nltk.download('omw-1.4')
nltk.download('punkt')

import warnings
warnings.filterwarnings('ignore', category=Warning)

import argparse
import stop_words
from nltk.corpus import stopwords
nltk.download('stopwords');
import nltk
import re
from nltk.stem.snowball import SnowballStemmer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.cluster import KMeans

def tokenize(column):
    tokens = nltk.word_tokenize(column)
    return [w for w in tokens if w.isalpha()]

def remove_stopwords(tokenized_column):
    # list of custom stopwords
    custom_stopwords = ['findings', 'comment', 'erratum', 'mri', 'gmc', 'conclusion', 'clinical history', 'impression']

    # Get the default set of stopwords from nltk
    nltk_stopwords = set(stopwords.words('english'))

    # Combine the two sets of stopwords
    all_stopwords = custom_stopwords + list(nltk_stopwords)

    # Perform text processing
    text = ' '.join(tokenized_column)
    text = re.sub(r'(?<!^)(?<!\.\s)(?<!\.\.\.\s)[A-Z][a-z]+', '', text) # Remove capitalized words that don't appear at the beginning of a sentence
    text = text.lower() # Convert to lowercase
    text = re.sub(r'[^\w\s]', '', text) # Remove punctuation
    text = re.sub(r'\d', '', text) # Remove digits
    text = re.sub(r'\s+', ' ', text) # Remove extra whitespaces
    text = text.strip() # Remove leading/trailing whitespaces
    
    # Remove any personal identifying information
    text = remove_personal_info(text)

    # Tokenize the cleaned text
    tokenized_cleaned_text = text.split()

    # Remove the stopwords from the tokenized_cleaned_text
    return [word for word in tokenized_cleaned_text if word not in all_stopwords]

def rejoin_words(tokenized_column):
    
    return ( " ".join(tokenized_column))

import nltk
from nltk.stem.snowball import SnowballStemmer
  
#the stemmer requires a language parameter

def snowballStemming(tokenized_column):
  sStemmer = SnowballStemmer(language='english')
  return[sStemmer.stem(word) for word in tokenized_column]

df_Reports = pd.read_csv('/content/MRIreports.csv')

df = pd.concat([df_Reports], axis = 1)

"""defines "Report Text" as "Original Text" (Doesn't use tumour column)"""
df['Original Text'] = df['Report Text']


df.head(10)

from collections import Counter

# Combine all reports into a single string
all_reports = " ".join(df['Original Text'].astype(str))

# Split the string into individual words
words = all_reports.lower().split()

# Count the frequency of each word
word_counts = Counter(words)

# Print the top 10 most common words
print(word_counts.most_common(10))

df['tokenized'] = df.apply(lambda x: tokenize(x['Original Text']), axis=1)
df[['Original Text', 'tokenized']].head(5000)

df['stopwords_removed'] = df.apply(lambda x: remove_stopwords(x['tokenized']), axis=1)
df[['Original Text', 'stopwords_removed']].head(5000)

df['snowballStemmed'] = df.apply(lambda x: snowballStemming(x['stopwords_removed']), axis=1)
df[['Original Text', 'snowballStemmed']].head(5000)

df['rejoined'] = df.apply(lambda x: rejoin_words(x['snowballStemmed']), axis=1)
df[['Original Text', 'rejoined']].head(5000)

df2 = df['rejoined'].head(5000)
df2.head(5000)

word_freq = Counter(" ".join(df['rejoined'].str.lower()).split()).most_common()

print(word_freq)

"""saves the rejoind preProcessed text to a new csv file"""
df3 = df2.to_csv('preProcessedText.csv', index=False)

corpus = list(df2)
print(df2)

from sentence_transformers import SentenceTransformer
embedder = SentenceTransformer('distilbert-base-nli-mean-tokens')

# Split the corpus into two groups - tumour reports and non-tumour reports
tumour_reports = []
non_tumour_reports = []
for report in corpus:
    if "tumour" in report:
        tumour_reports.append(report)
    else:
        non_tumour_reports.append(report)

print(tumour_reports)

# Encode the corpus using the pre-trained model
tumour_embeddings = embedder.encode(tumour_reports)
non_tumour_embeddings = embedder.encode(non_tumour_reports)

for report in tumour_reports:
    if "tumour" in report:
        print(report)

# Cluster the tumour and non-tumour reports separately using KMeans
num_clusters = 1
tumour_clustering_model = KMeans(n_clusters=num_clusters)
non_tumour_clustering_model = KMeans(n_clusters=num_clusters)

tumour_clustering_model.fit(tumour_embeddings)
non_tumour_clustering_model.fit(non_tumour_embeddings)

tumour_cluster_assignment = tumour_clustering_model.labels_
non_tumour_cluster_assignment = non_tumour_clustering_model.labels_

# Print the cluster for the tumour reports
tumour_clustered_sentences = [[] for i in range(num_clusters)]
for sentence_id, cluster_id in enumerate(tumour_cluster_assignment):
    tumour_clustered_sentences[cluster_id].append(tumour_reports[sentence_id])

for i, cluster in enumerate(tumour_clustered_sentences):
    print("Tumour Cluster ")
    print(cluster)
    print("")

# Print the cluster for the non-tumour reports
non_tumour_clustered_sentences = [[] for i in range(num_clusters)]
for sentence_id, cluster_id in enumerate(non_tumour_cluster_assignment):
    non_tumour_clustered_sentences[cluster_id].append(non_tumour_reports[sentence_id])

    for i, cluster in enumerate(non_tumour_clustered_sentences):
        print("Non-tumour Cluster ")
        print(cluster)
        print("")

def print_clustered_sentences(reports, cluster_assignment, cluster_label):
    clustered_sentences = {i: [] for i in range(max(cluster_assignment) + 1)}
    for sentence_id, cluster_id in enumerate(cluster_assignment):
        clustered_sentences[cluster_id].append(reports[sentence_id])

    for cluster_id, cluster in clustered_sentences.items():
        print(f"{cluster_label} {cluster_id}:")
        print(cluster)
        print("")

# Visualize the clusters
tumour_labels = tumour_clustering_model.labels_
non_tumour_labels = non_tumour_clustering_model.labels_

plt.figure(figsize=(32, 16))

plt.scatter(tumour_embeddings[tumour_labels==0, 0], tumour_embeddings[tumour_labels==0, 1], s=200, c='orange', label ='Tumour Cluster 1')

plt.scatter(non_tumour_embeddings[non_tumour_labels==0, 0], non_tumour_embeddings[non_tumour_labels==0, 1], s=50, c='blue', label ='Non-Tumour Cluster 1')

plt.title("Tumour and Non-Tumour Cluster", fontsize=20)
plt.xlabel("Dimension 1", fontsize=14)
plt.ylabel("Dimension 2", fontsize=14)
plt.legend(fontsize=14)
plt.show()

tumour_labels = tumour_clustering_model.labels_
non_tumour_labels = non_tumour_clustering_model.labels_

plt.figure(figsize=(16, 8))

# Plot tumour clusters
plt.subplot(1, 2, 1)
plt.scatter(tumour_embeddings[tumour_labels==0, 0], tumour_embeddings[tumour_labels==0, 1], s=100, c='orange', label ='Tumour Cluster 1')
plt.title("Pos-Tumour Report Cluster", fontsize=18)
plt.legend(fontsize=12)
plt.xlabel("Dimension 1", fontsize=14)
plt.ylabel("Dimension 2", fontsize=14)

# Plot non-tumour clusters
plt.subplot(1, 2, 2)
plt.scatter(non_tumour_embeddings[non_tumour_labels==0, 0], non_tumour_embeddings[non_tumour_labels==0, 1], s=100, c='blue', label ='Non-Tumour Cluster 1')
plt.title("Neg-Tumour Report Cluster", fontsize=18)
plt.legend(fontsize=12)
plt.xlabel("Dimension 1", fontsize=14)
plt.ylabel("Dimension 2", fontsize=14)

plt.tight_layout()
plt.show()

# Save the clusters for the tumour reports to CSV file
for i, cluster in enumerate(tumour_clustered_sentences):
    cluster_df = pd.DataFrame(cluster, columns=['Pos-Tumour Report'])
    cluster_df.to_csv(f'pos_tumour_cluster.csv', index=False)

# Save the clusters for the non-tumour reports to CSV file
for i, cluster in enumerate(non_tumour_clustered_sentences):
    cluster_df = pd.DataFrame(cluster, columns=['Neg-Tumour Report'])
    cluster_df.to_csv(f'neg_tumour_cluster.csv', index=False)



from wordcloud import WordCloud

def generate_wordclouds(clustered_sentences, label):
    for i, cluster in enumerate(clustered_sentences):
        wc = ' '.join(cluster)
        wordcloud = WordCloud(width=800, height=500, random_state=21, max_font_size=110).generate(wc)
        fig = plt.figure(figsize=(10, 7))
        plt.imshow(wordcloud, interpolation="bilinear")
        plt.axis('off')
        plt.title(f"{label} Word Cloud")
        plt.savefig(f"{label.lower()}_cluster_wordcloud.png")
        
generate_wordclouds(tumour_clustered_sentences, "Tumour")
generate_wordclouds(non_tumour_clustered_sentences, "Non-tumour")